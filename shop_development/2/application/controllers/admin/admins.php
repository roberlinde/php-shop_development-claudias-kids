<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:32:07 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/controllers/admin/admins.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Admins extends CI_Controller {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('user_id') < 1)
		{
			redirect('welcome/verify', 'refresh');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * index()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		$data['title']	= "Manage Users";
		$data['main']	= 'admin_admins_home';
		$data['admins']	= $this->admins_model->get_all_users();

		$this->load->vars($data);
		$this->load->view('dashboard');  
	}  

	// --------------------------------------------------------------------

	/**
	 * create()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function create()
	{
		if ($this->input->post('user_name'))
		{
			$this->admins_model->add_user();

			$this->session->set_flashdata('message', 'User created');

			redirect('admin/admins/index', 'refresh');
		}
		else
		{
			$data['title']	= "Create User";
			$data['main']	= 'admin_admins_create';

			$this->load->vars($data);
			$this->load->view('dashboard');    
		} 
	}

	// --------------------------------------------------------------------

	/**
	 * edit()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function edit($id = 0)
	{
		if ($this->input->post('user_name'))
		{
			$this->admins_model->update_user();

			$this->session->set_flashdata('message', 'User updated');

			redirect('admin/admins/index', 'refresh');
		}
		else
		{
			$data['title'] = "Edit User";
			$data['main']  = 'admin_admins_edit';
			$data['admin'] = $this->admins_model->get_user($id);

			if ( ! count($data['admin']))
			{
				redirect('admin/admins/index', 'refresh');
			}

			$this->load->vars($data);
			$this->load->view('dashboard');    
		}
	}

	// --------------------------------------------------------------------

	/**
	 * delete()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete($id)
	{
		$this->admins_model->delete_user($id);

		$this->session->set_flashdata('message', 'User deleted');

		redirect('admin/admins/index', 'refresh');
	}
  
}


// ------------------------------------------------------------------------
/* End of file admins.php */
/* Location: ./application/contollers/admin/admins.php */