<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:32:45 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/controllers/admin/categories.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Categories extends CI_Controller {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('user_id') < 1)
		{
			redirect('welcome/verify', 'refresh');
		}
	}
  
	// --------------------------------------------------------------------

	/**
	 * index()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		$data['title']		= "Manage Categories";
		$data['main']		= 'admin_cat_home';
		$data['categories']	= $this->categories_model->get_all_categories();

		$this->load->vars($data);
		$this->load->view('dashboard');  
	}  

	// --------------------------------------------------------------------

	/**
	 * create()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function create()
	{
		if ($this->input->post('name'))
		{
			$this->categories_model->add_category();

			$this->session->set_flashdata('message', 'Category created');

			redirect('admin/categories/index', 'refresh');
		}
		else
		{
			$data['title']		= "Create Category";
			$data['main']		= 'admin_cat_create';
			$data['categories']	= $this->categories_model->get_top_categories();

			$this->load->vars($data);
			$this->load->view('dashboard');    
		} 
	}

	// --------------------------------------------------------------------

	/**
	 * edit()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function edit($id = 0)
	{
		if ($this->input->post('name'))
		{
			$this->categories_model->update_category();

			$this->session->set_flashdata('message', 'Category updated');

			redirect('admin/categories/index', 'refresh');
		}
		else
		{
			$data['title']		= "Edit Category";
			$data['main']		= 'admin_cat_edit';
			$data['category']	= $this->categories_model->get_category($id);
			$data['categories']	= $this->categories_model->get_top_categories();

			if ( ! count($data['category']))
			{
				redirect('admin/categories/index', 'refresh');
			}

			$this->load->vars($data);
			$this->load->view('dashboard');    
		}
	}

	// --------------------------------------------------------------------

	/**
	 * delete()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete($id)
	{
		$this->categories_model->delete_category($id);

		$orphans = $this->categories_model->check_orphans($id);

		if (count($orphans))
		{
			$this->session->set_userdata('orphans', $orphans);

			redirect('admin/categories/reassign/'.$id, 'refresh');	
		}
		else
		{
			$this->session->set_flashdata('message', 'Category deleted');

			redirect('admin/categories/index', 'refresh');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * export()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function export()
	{
		$this->load->helper('download');

		$csv  = $this->categories_model->export_csv();

		$name = "category_export.csv";

		force_download($name, $csv);
	}

	// --------------------------------------------------------------------

	/**
	 * reassign()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function reassign($id = 0)
	{
		if ($_POST)
		{
			$this->products_model->reassign_products();

			$this->session->set_flashdata('message', 'Category deleted and products reassigned');

			redirect('admin/categories/index', 'refresh');
		}
		else
		{
			$data['category']	= $this->categories_model->get_category($id);
			$data['title']		= "Reassign Products";
			$data['main']		= 'admin_cat_reassign';
			$data['categories']	= $this->categories_model->get_categories_dropdown();

			$this->load->vars($data);
			$this->load->view('dashboard');    	
		}	
	}

}


// ------------------------------------------------------------------------
/* End of file categories.php */
/* Location: ./application/controllers/admin/categories.php */