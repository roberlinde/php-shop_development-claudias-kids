<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:33:16 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/controllers/admin/colors.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Colors extends CI_Controller {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

  		if ($this->session->userdata('user_id') < 1)
		{
			redirect('welcome/verify', 'refresh');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * index()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		$data['title']  = "Manage Colors";
		$data['main']   = 'admin_colors_home';
		$data['colors'] = $this->colors_model->get_all_colors();

		$this->load->vars($data);
		$this->load->view('dashboard');  
	}  

	// --------------------------------------------------------------------

	/**
	 * create()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function create()
	{
		if ($this->input->post('name'))
		{
			$this->colors_model->create_color();

			$this->session->set_flashdata('message', 'Color created');

			redirect('admin/colors/index', 'refresh');
		}
		else
		{
			$data['title'] = "Create Color";
			$data['main']  = 'admin_colors_create';

			$this->load->vars($data);
			$this->load->view('dashboard');    
		} 
	}

	// --------------------------------------------------------------------

	/**
	 * edit()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function edit($id = 0)
	{
		if ($this->input->post('name'))
		{
			$this->colors_model->update_color();

			$this->session->set_flashdata('message', 'Color updated');

			redirect('admin/colors/index','refresh');
		}
		else
		{
			$data['title'] = "Edit Color";
			$data['main']  = 'admin_colors_edit';
			$data['color'] = $this->colors_model->get_color($id);

			if ( ! count($data['color']))
			{
				redirect('admin/colors/index', 'refresh');
			}

			$this->load->vars($data);
			$this->load->view('dashboard');    
		}
	}

	// --------------------------------------------------------------------

	/**
	 * delete()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete($id)
	{
		$this->colors_model->delete_color($id);

		$this->session->set_flashdata('message', 'Color deleted');

		redirect('admin/colors/index', 'refresh');
	}

}


// ------------------------------------------------------------------------
/* End of file colors.php */
/* Location: ./application/controllers/admin/colors.php */