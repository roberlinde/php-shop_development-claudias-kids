<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:25:31 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/controllers/admin/dashboard.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Dashboard extends CI_Controller {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('user_id') < 1)
		{
			redirect('welcome/verify', 'refresh');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * index()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		$data['title'] = "Dashboard Home";
		$data['main']  = 'admin_home';

		$this->load->vars($data);
		$this->load->view('dashboard');
	}

	// --------------------------------------------------------------------

	/**
	 * logout()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function logout()
	{
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_name');

		$this->session->set_flashdata('error', "You've been logged out!");

		redirect('welcome/verify', 'refresh');
	}
 
}


// ------------------------------------------------------------------------
/* End of file dashboard.php */
/* Location: ./application/controllers/admin/dashboard.php */