<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:34:03 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/controllers/admin/pages.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Pages extends CI_Controller {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('user_id') < 1)
		{
			redirect('welcome/verify', 'refresh');
		}
    
		$this->tinyMce = '
		<!-- TinyMCE -->
			<script type="text/javascript" src="'. base_url().'js/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript">
				tinyMCE.init({
					// General options
					mode : "textareas",
					theme : "simple"
				});
			</script>
		<!-- /TinyMCE -->
		';
	}

	// --------------------------------------------------------------------

	/**
	 * index()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		$data['title'] = "Manage Pages";
		$data['main']  = 'admin_pages_home';
		$data['pages'] = $this->pages_model->get_all_pages();

		$this->load->vars($data);
		$this->load->view('dashboard');  
	}  

	// --------------------------------------------------------------------

	/**
	 * create()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function create()
	{
		if ($this->input->post('name'))
		{
			$this->pages_model->add_page();

			$this->session->set_flashdata('message', 'Page created');

			redirect('admin/pages/index', 'refresh');
		}
		else
		{
			$data['title'] = "Create Page";
			$data['main']  = 'admin_pages_create';

			$this->load->vars($data);
			$this->load->view('dashboard');    
		} 
	}

	// --------------------------------------------------------------------

	/**
	 * edit()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function edit($id = 0)
	{
		if ($this->input->post('name'))
		{
			$this->pages_model->update_page();

			$this->session->set_flashdata('message', 'Page updated');

			redirect('admin/pages/index', 'refresh');
		}
		else
		{
			$data['title'] = "Edit Page";
			$data['main']  = 'admin_pages_edit';
			$data['page']  = $this->pages_model->get_page($id);

			if ( ! count($data['page']))
			{
				redirect('admin/pages/index', 'refresh');
			}

			$this->load->vars($data);
			$this->load->view('dashboard');    
		}
	}

	// --------------------------------------------------------------------

	/**
	 * delete()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete($id)
	{
		$this->pages_model->delete_page($id);

		$this->session->set_flashdata('message', 'Page deleted');

		redirect('admin/pages/index', 'refresh');
	}

}


// ------------------------------------------------------------------------
/* End of file pages.php */
/* Location: ./application/controllers/admin/pages.php */