<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:34:31 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/controllers/admin/products.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Products extends CI_Controller {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('user_id') < 1)
		{
			redirect('welcome/verify', 'refresh');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * index()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		$data['title']		= "Manage Products";
		$data['main']		= 'admin_product_home';
		$data['products']	= $this->products_model->get_all_products();
		$data['categories']	= $this->categories_model->get_categories_dropdown();

		$this->load->vars($data);
		$this->load->view('dashboard');  
	}

	// --------------------------------------------------------------------

	/**
	 * create()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function create()
	{
		if ($this->input->post('name'))
		{
			$this->products_model->add_product();

			$this->session->set_flashdata('message', 'Product created');

			redirect('admin/products/index', 'refresh');
		}
		else
		{
			$data['title']		= "Create Product";
			$data['main']		= 'admin_product_create';
			$data['categories'] = $this->categories_model->get_categories_dropdown();
			$data['colors']		= $this->colors_model->get_active_colors();
			$data['sizes']		= $this->sizes_model->get_active_sizes();

			$this->load->vars($data);
			$this->load->view('dashboard');    
		} 
	}

	// --------------------------------------------------------------------

	/**
	 * edit()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function edit($id = 0)
	{
		if ($this->input->post('name'))
		{
			$this->products_model->update_product();

			$this->session->set_flashdata('message', 'Product updated');

			redirect('admin/products/index', 'refresh');
		}
		else
		{
			$data['title']			 = "Edit Product";
			$data['main']			 = 'admin_product_edit';
			$data['product']		 = $this->products_model->get_product($id);
			$data['categories']		 = $this->categories_model->get_categories_dropdown();
			$data['assigned_colors'] = $this->products_model->get_assigned_colors($id);
			$data['assigned_sizes']	 = $this->products_model->get_assigned_sizes($id);
			$data['colors']			 = $this->colors_model->get_active_colors();
			$data['sizes']			 = $this->sizes_model->get_active_sizes();

			if ( ! count($data['product']))
			{
				redirect('admin/products/index', 'refresh');
			}

			$this->load->vars($data);
			$this->load->view('dashboard');    
		}
	}

	// --------------------------------------------------------------------

	/**
	 * delete()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete($id)
	{
		$this->products_model->delete_product($id);

		$this->session->set_flashdata('message', 'Product deleted');

		redirect('admin/products/index', 'refresh');
	}

	// --------------------------------------------------------------------

	/**
	 * batch_mode()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function batch_mode()
	{
		$this->products_model->batch_update();

		redirect('admin/products/index', 'refresh');
	}

	// --------------------------------------------------------------------

	/**
	 * export()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function export()
	{
		$this->load->helper('download');

		$csv = $this->products_model->export_csv();

		$name = "product_export.csv";

		force_download($name, $csv);
	}

	// --------------------------------------------------------------------

	/**
	 * import()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function import()
	{
		if ($this->input->post('csvinit'))
		{
			$data['csv']   = $this->products_model->import_csv();
			$data['title'] = "Preview Import Data";
			$data['main']  = 'admin_product_csv';

			$this->load->vars($data);
			$this->load->view('dashboard');
		}

		elseif ($this->input->post('csvgo'))
		{
			if (preg_match("/finalize/i", $this->input->post('submit')))
			{
				$this->products_model->csv_to_db();

				$this->session->set_flashdata('message', 'CSV data imported');
			}
			else
			{
				$this->session->set_flashdata('message', 'CSV data import cancelled');
			}

			redirect('admin/products/index', 'refresh');
		}  	
	}

}


// ------------------------------------------------------------------------
/* End of file products.php */
/* Location: ./application/controllers/admin/products.php */