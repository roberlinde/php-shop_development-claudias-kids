<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:35:08 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/controllers/admin/sizes.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Sizes extends CI_Controller {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('user_id') < 1)
		{
			redirect('welcome/verify', 'refresh');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * index()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		$data['title'] = "Manage Sizes";
		$data['main']  = 'admin_sizes_home';
		$data['sizes'] = $this->sizes_model->get_all_sizes();

		$this->load->vars($data);
		$this->load->view('dashboard');  
	}

	// --------------------------------------------------------------------

	/**
	 * create()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function create()
	{
		if ($this->input->post('name'))
		{
			$this->sizes_model->create_size();

			$this->session->set_flashdata('message', 'Size created');

			redirect('admin/sizes/index', 'refresh');
		}
		else
		{
			$data['title'] = "Create Size";
			$data['main']  = 'admin_sizes_create';

			$this->load->vars($data);
			$this->load->view('dashboard');    
		}
	}

	// --------------------------------------------------------------------

	/**
	 * edit()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function edit($id = 0)
	{
		if ($this->input->post('name'))
		{
			$this->sizes_model->update_size();

			$this->session->set_flashdata('message', 'Size updated');

			redirect('admin/sizes/index', 'refresh');
		}
		else
		{
			$data['title'] = "Edit Size";
			$data['main']  = 'admin_sizes_edit';
			$data['size']  = $this->sizes_model->get_size($id);

			if ( ! count($data['size']))
			{
				redirect('admin/sizes/index', 'refresh');
			}

			$this->load->vars($data);
			$this->load->view('dashboard');    
		}
	}

	// --------------------------------------------------------------------

	/**
	 * delete()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete($id)
	{
		$this->sizes_model->delete_size($id);

		$this->session->set_flashdata('message', 'Size deleted');

		redirect('admin/sizes/index', 'refresh');
	}

}


// ------------------------------------------------------------------------
/* End of file sizes.php */
/* Location: ./application/controllers/admin/sizes.php */