<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:35:39 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/controllers/admin/subscribers.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Subscribers extends CI_Controller {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('user_id') < 1)
		{
			redirect('welcome/verify', 'refresh');
		}

		$this->tinyMce = '
			<!-- TinyMCE -->
			<script type="text/javascript" src="'.base_url().'js/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript">
				tinyMCE.init({
					// General options
					mode : "textareas",
					theme : "simple"
				});
			</script>
			<!-- /TinyMCE -->
			';
	}

	// --------------------------------------------------------------------

	/**
	 * index()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		$data['title']		 = "Manage Subscribers";
		$data['main']		 = 'admin_subs_home';
		$data['subscribers'] = $this->subscribers_model->get_all_subscribers();

		$this->load->vars($data);
		$this->load->view('dashboard');  
	}

	// --------------------------------------------------------------------

	/**
	 * send_email()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function send_email()
	{
		$this->load->helper('file');

		if ($this->input->post('subject'))
		{
			$test		= $this->input->post('test');
			$subject	= $this->input->post('subject');
			$msg		= $this->input->post('message');

			if ($test)
			{
				$this->email->clear();
				$this->email->from('claudia@example.com', 'ClaudiasKids.net');
				$this->email->to('myerman@gmail.com');
				$this->email->subject($subject);
				$this->email->message($msg);		
				$this->email->send();

				$this->session->set_flashdata('message', "Test email sent");

				write_file('/tmp/email.log', $subject ."|||".$msg);

				redirect('admin/subscribers/send_email', 'refresh'); 
			}
			else
			{
				$subs = $this->subscribers_model->get_all_subscribers();

				foreach ($subs as $key => $list)
				{
					$unsub = "<p><a href='". base_url()."welcome/unsubscribe/".$list['id']. "'>Unsubscribe</a></p>";

					$this->email->clear();
					$this->email->from('claudia@example.com', 'ClaudiasKids.net');
					$this->email->to($list['email']);
					$this->email->bcc('claudia@example.com'); 
					$this->email->subject($subject);
					$this->email->message($msg.$unsub);		
					$this->email->send();	
				}

				$this->session->set_flashdata('message', count($subs) . " emails sent");
			}

			redirect('admin/subscribers/index', 'refresh');
		}
		else
		{
			if ($this->session->flashdata('message') == "Test email sent")
			{
				$lastemail = read_file('/tmp/email.log');

				list($subj, $msg) = explode("|||", $lastemail);

				$data['subject'] = $subj;
				$data['msg']	 = $msg;
			}
			else
			{
				$data['subject'] = '';
				$data['msg']	 = '';
			}

			$data['title'] = "Send Email";
			$data['main']  = 'admin_subs_mail';

			$this->load->vars($data);
			$this->load->view('dashboard');  			
		}
	}

	// --------------------------------------------------------------------

	/**
	 * delete()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete($id)
	{
		$this->subscribers_model->remove_subscriber($id);

		$this->session->set_flashdata('message', 'Subscriber deleted');

		redirect('admin/subscribers/index', 'refresh');
	}

}


// ------------------------------------------------------------------------
/* End of file subsribers.php */
/* Location: ./application/controllers/admin/subscribers.php */