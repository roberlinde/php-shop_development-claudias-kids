<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 9:47:43 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/controllers/welcome.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Welcome extends CI_Controller {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();


		$this->output->enable_profiler(FALSE);
	}

	// --------------------------------------------------------------------

	/**
	 * index()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function index()
	{
		// ----------------------------------------------------------------------

		/**
		 * This will generate your new password, delete this when finished!!!
		 * 
		 * remove remarks
		 */
		
		// ----------------------  user  -  password
		$this->password_generator('admin', 'k202509');
		
		// Do not forget to delete the above lines
		// and the method at the end of this file!!!
		// ----------------------------------------------------------------------


		//$this->output->cache(30);

		$data['title']		= "Welcome to Claudia's Kids";
		$data['navlist']	= $this->categories_model->get_categories_nav();
		$data['mainf']		= $this->products_model->get_main_feature();
		
		$skip = $data['mainf']['id'];
		
		$data['sidef']	= $this->products_model->get_random_products(3, $skip);
		$data['main']	= 'home';
	
		$this->load->vars($data);
		$this->load->view('template');
	}

	// --------------------------------------------------------------------

	/**
	 * cat()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function cat($id)
	{
		$cat = $this->categories_model->get_category($id);

		if ( ! count($cat))
		{
			redirect('welcome/index', 'refresh');
		}

		$data['title'] = "Claudia's Kids | " . $cat['name'];
	
		if ($cat['parent_id'] < 1)
		{
			// show other categories
			$data['listing']	= $this->categories_model->get_sub_categories($id);
			$data['level']		= 1;
		}
		else
		{
			// show products
			$data['listing']	= $this->products_model->get_products_by_category($id);
			$data['level']		= 2;
		}

		$data['category']	= $cat;
		$data['main']		= 'category';
		$data['navlist']	= $this->categories_model->get_categories_nav();

		$this->load->vars($data);
		$this->load->view('template');
	}

	// --------------------------------------------------------------------

	/**
	 * product()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function product($id)
	{
		$product = $this->products_model->get_product($id);
		
		if ( ! count($product))
		{
			redirect('welcome/index', 'refresh');
		}

		$data['group_list']	= $this->products_model->get_products_by_group(3, $product['grouping'], $id);
		$data['product']	= $product;
		$data['title']		= "Claudia's Kids | " . $product['name'];
		$data['main']		= 'product';

		$data['navlist']			= $this->categories_model->get_categories_nav();
		$data['assigned_colors']	= $this->products_model->get_assigned_colors($id);
		$data['assigned_sizes']		= $this->products_model->get_assigned_sizes($id);
		$data['colors']				= $this->colors_model->get_active_colors();
		$data['sizes']				= $this->sizes_model->get_active_sizes();

		$this->load->vars($data);
		$this->load->view('template');
	}

	// --------------------------------------------------------------------

	/**
	 * cart()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function cart($product_id = 0)
	{
		if ($product_id > 0)
		{
			$full_product = $this->products_model->get_product($product_id);

			$this->orders_model->update_cart($product_id, $full_product);
			
			redirect('welcome/product/'.$product_id, 'refresh');
		}
		else
		{
			$data['title'] = "Claudia's Kids | Shopping Cart";
			
			if (count($_SESSION['cart']))
			{
				$data['main']		= 'shoppingcart';
				$data['navlist']	= $this->categories_model->get_categories_nav();

				$this->load->vars($data);
				$this->load->view('template');	
			}
			else
			{
				redirect('welcome/index', 'refresh');
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * ajax_cart()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function ajax_cart()
	{
		$this->orders_model->update_cart_ajax($this->input->post('ids', TRUE));
	}

	// --------------------------------------------------------------------

	/**
	 * ajax_cart_remove()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function ajax_cart_remove()
	{
		$this->orders_model->remove_line_item($this->input->post('id', TRUE));
	}

	// --------------------------------------------------------------------

	/**
	 * check_out()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function check_out()
	{
		$this->orders_model->verify_cart();

		$data['main']		= 'confirmorder';
		$data['title']		= "Claudia's Kids | Order Confirmation";
		$data['navlist']	= $this->categories_model->get_categories_nav();

		$this->load->vars($data);
		$this->load->view('template');   	
	}

	// --------------------------------------------------------------------

	/**
	 * search()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function search()
	{
		if ($this->input->post('term'))
		{
			$data['results'] = $this->products_model->search($this->input->post('term', TRUE));
		}
		else
		{
			redirect('welcome/index', 'refresh');
		}

		$data['main']		= 'search';
		$data['title']		= "Claudia's Kids | Search Results";
		$data['navlist']	= $this->categories_model->get_categories_nav();

		$this->load->vars($data);
		$this->load->view('template');  
	}

	// --------------------------------------------------------------------

	/**
	 * verify()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function verify()
	{
		if ($this->input->post('user_name', TRUE))
		{
			$user_name = $this->input->post('user_name', TRUE);
			$password  = $this->input->post('password', TRUE);

			$this->admins_model->verify_user($user_name, $password);
		
			if ($this->session->userdata('user_id') > 0)
			{
				redirect('admin/dashboard', 'refresh');
			}
		}

		$data['main']		= 'login';
		$data['title']		= "Claudia's Kids | Admin Login";
		$data['navlist']	= $this->categories_model->get_categories_nav();

		$this->load->vars($data);
		$this->load->view('template');  
	}

	// --------------------------------------------------------------------

	/**
	 * pages()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function pages($path)
	{
    	$page = $this->pages_model->get_page_path($path);

		$data['main']		= 'page';
		$data['title']		= $page['name'];
		$data['page']		= $page;
		$data['navlist']	= $this->categories_model->get_categories_nav();
	
		$this->load->vars($data);
		$this->load->view('template'); 
	}

	// --------------------------------------------------------------------

	/**
	 * subscribe()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function subscribe()
	{
  		if ($this->input->post('email', TRUE))
  		{
	  		$this->load->helper('email');

			if ( ! valid_email($this->input->post('email', TRUE)))
			{
				$this->session->set_flashdata('subscribe_msg', 'Invalid email. Please try again!');

				redirect('welcome/index', 'refresh');
			}

			$this->subscribers_model->create_subscriber();

			$this->session->set_flashdata('subscribe_msg', 'Thanks for subscribing!');

			redirect('welcome/index', 'refresh');
  		}
  		else
  		{
			$this->session->set_flashdata('subscribe_msg', "You didn't fill out the form!");

			redirect('welcome/index', 'refresh');
	  	}
	}

	// --------------------------------------------------------------------

	/**
	 * un_subscribe()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function un_subscribe($id)
	{
		$this->subscribers_model->delete_subscriber($id);

		$this->session->set_flashdata('subscribe_msg', 'You have been unsubscribed!');
	
		redirect('welcome/index', 'refresh');
	}

	// ----------------------------------------------------------------------
	// Make sure you delete this methos when finished!!!!
	// ----------------------------------------------------------------------

	/**
	 * password_generator()
	 *
	 * * Place this method at the bottom of the Controller!
	 * 
	 * NOTE: You can save this and the above code for later use!
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	void
	 */
    public function password_generator($user, $password)
    {
		$pass = $this->admins_model->hash_password($user, $password);
		
		echo $pass . '<br /><br />';

		exit('New Password Created. Please delete the password_generator  method!!!!');
    }

}


// ------------------------------------------------------------------------
/* End of file welcome.php */
/* Location: ./application/controller/welcome.php */