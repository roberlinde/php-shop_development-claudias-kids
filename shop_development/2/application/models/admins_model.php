<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:23:13 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/models/admins_model.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Admins_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * verify_user()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	void
	 */
	public function verify_user($user, $password)
	{
		$this->db->select('id, user_name, password');
		$this->db->where('user_name', $user);
		$this->db->where('password', $this->hash_password($user, $password));
		$this->db->where('status', 'active');
		$this->db->limit(1);

		$query = $this->db->get('admins');

		if ($query->num_rows() > 0)
		{
			$row = $query->row_array();

			$this->session->set_userdata('user_id', $row['id']);
			$this->session->set_userdata('user_name', $row['user_name']);
		}
		else
		{
			$this->session->set_flashdata('error', 'Sorry, your username or password is incorrect!');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * get_user()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_user($id)
	{
      $data		= array();
      $options	= array('id' => $id);

      $query = $this->db->get_where('admins', $options, 1);

      if ($query->num_rows() > 0)
      {
        $data = $query->row_array();
      }

      $query->free_result();
      
      return $data;
	}

	// --------------------------------------------------------------------

	/**
	 * get_all_users()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_all_users()
	{
		$data = array();

		$query = $this->db->get('admins');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$query->free_result();    

		return $data; 	
	}

	// --------------------------------------------------------------------

	/**
	 * add_user()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function add_user()
	{
		$data = array('user_name'	=> $this->input->post('user_name', TRUE),
					  'email'		=> $this->input->post('email', TRUE),
					  'status'		=> $this->input->post('status', TRUE),
					  'password'	=> $this->hash_password($this->input->post('user_name', TRUE), $this->input->post('password', TRUE))
					 );

		$this->db->insert('admins', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * update_user()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function update_user()
	{
		$data = array('user_name'	=> $this->input->post('user_name', TRUE),
					  'email'		=> $this->input->post('email', TRUE),
					  'status'		=> $this->input->post('status', TRUE),
					  'password'	=> $this->hash_password($this->input->post('user_name', TRUE), $this->input->post('password', TRUE))
					 );

		$this->db->where('id', $this->input->post('id', TRUE));
		$this->db->update('admins', $data);	
	}

	// --------------------------------------------------------------------

	/**
	 * delete_user()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete_user($id)
	{
		$data = array('status' => 'inactive');

		$this->db->where('id', $id);
		$this->db->update('admins', $data);	
	}

	// -----------------------------------------------------------------------

	/**
	 * hash_password()
	 * 
	 * Hashes the users user_name, password and CI 32-bit encryption key 
	 * using SHA-512. I place this in my user_model.
	 * 
	 * You can also pass in the user and password fields to
	 * this method to generate the encryption key then ech the returned value.
	 * 
	 * NOTE: The Database password field needs to be varchar(128)
	 * 
	 * @access	public
	 * @param	string	- $user_name
	 * @param	string	- $password
	 * @retrun	string	- the 128 char encrypted password
	 */
	public function hash_password($user_name, $password)
	{
		$salt = config_item('encryption_key');

		return hash('SHA512', $user_name . $password . $salt);
	}

}


// ------------------------------------------------------------------------
/* End of file admins_model.php */
/* Location: ./application/models/admins_model.php */