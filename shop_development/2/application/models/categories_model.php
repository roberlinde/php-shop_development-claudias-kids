<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:24:19 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/models/categories_model.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Categories_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * get_category()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_category($id)
	{
		$data = array();

		$options = array('id' => $id);

		$query = $this->db->get_where('categories', $options, 1);

		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}

		$query->free_result();

		return $data;    
	}

	// --------------------------------------------------------------------

	/**
	 * get_all_categories()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_all_categories()
	{
		$data = array();

		$query = $this->db->get('categories');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$query->free_result();

		return $data; 
	}

	// --------------------------------------------------------------------

	/**
	 * get_sub_categories()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_sub_categories($cat_id)
	{
		$data = array();

		$this->db->select('id, name, short_desc');

		$this->db->where('parent_id', $cat_id);
		$this->db->where('status', 'active');

		$this->db->order_by('name', 'asc');

		$query = $this->db->get('categories');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$sql = "select thumb_nail as src
						from products
						where category_id=" . $row['id'] . "
						And status='active'
						order by rand() limit 1";

				$query_2 = $this->db->query($sql);

				if ($query_2->num_rows() > 0)
				{
					$thumb = $query_2->row_array();

					$thumb_nail = $thumb['src'];
				}
				else
				{
					$thumbnail = '';
				}

				$query_2->free_result();

				$data[] = array(
					'id'			=> $row['id'],
					'name'			=> $row['name'],
					'short_desc'	=> $row['short_desc'],
					'thumb_nail'	=> $thumb_nail
				);
			}
		}

		$query->free_result();
    
		return $data;
	}

	// --------------------------------------------------------------------

	/**
	 * get_categories_nav()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_categories_nav()
	{
		$data = array();

		$this->db->select('id, name, parent_id');
		$this->db->where('status', 'active');
		$this->db->order_by('parent_id', 'asc');
		$this->db->order_by('name', 'asc');
		$this->db->group_by('parent_id, id');

		$query = $this->db->get('categories');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				if ($row->parent_id > 0)
				{
					$data[0][$row->parent_id]['children'][$row->id] = $row->name;
				}
				else
				{
					$data[0][$row->id]['name'] = $row->name;
				}
			}
		}

		$query->free_result();

		return $data; 
	}

	// --------------------------------------------------------------------

	/**
	 * get_categories_dropdown()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_categories_dropdown()
	{
		$data = array();

		$this->db->select('id, name');
		$this->db->where('parent_id !=', 0);

		$query = $this->db->get('categories');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[$row['id']] = $row['name'];
			}
		}

		$query->free_result();

		return $data; 
	}

	// --------------------------------------------------------------------

	/**
	 * get_top_categories()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_top_categories()
	{
		$data[0] = 'root';

		$this->db->where('parent_id', 0);

		$query = $this->db->get('categories');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[$row['id']] = $row['name'];
			}
		}

		$query->free_result();

		return $data; 
	}	

	// --------------------------------------------------------------------

	/**
	 * add_category()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function add_category()
	{
		$data = array( 
			'name'			=> $this->input->post('name'),
			'short_desc'	=> $this->input->post('short_desc'),
			'long_desc'		=> $this->input->post('long_desc'),
			'status'		=> $this->input->post('status'),
			'parent_id'		=> $this->input->post('parent_id')
		);

		$this->db->insert('categories', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * update_category()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function update_category()
	{
		$data = array(
			'name'			=> $this->input->post('name'),
			'short_desc'	=> $this->input->post('short_desc'),
			'long_desc'		=> $this->input->post('long_desc'),
			'status'		=> $this->input->post('status'),
			'parent_id'		=> $this->input->post('parent_id')
		);

		$this->db->where('id', $this->input->post('id'));

		$this->db->update('categories', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * delete_category()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete_category($id)
	{
		$data = array('status' => 'inactive');

		$this->db->where('id', $id);
		$this->db->update('categories', $data);
	}

	// --------------------------------------------------------------------

	/**
	 * export_csv()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function export_csv()
	{
		$this->load->dbutil();

		$query = $this->db->query("select * from categories");

		return $this->dbutil->csv_from_result($query, ",", "\n");
	}

	// --------------------------------------------------------------------

	/**
	 * check_orphans()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function check_orphans($id)
	{
		$data = array();

		$this->db->select('id, name');
		$this->db->where('category_id', $id);

		$query = $this->db->get('products');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[$row['id']] = $row['name'];
			}
		}

		$query->free_result();

		return $data; 
	}

}


// ------------------------------------------------------------------------
/* End of file categories_model.php */
/* Location: ./application/models/categories_model.php */