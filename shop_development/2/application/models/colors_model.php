<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:24:52 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/models/colors_model.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Colors_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * get_color()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_color($id)
	{
		$data = array();

		$options = array('id' => $id);

		$query = $this->db->get_where('colors', $options, 1);

		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}

		$query->free_result();    

		return $data;    
	}

	// --------------------------------------------------------------------

	/**
	 * get_all_colors()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_all_colors()
	{
		$data = array();

		$query = $this->db->get('colors');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$query->free_result();  

		return $data; 
	}

	// --------------------------------------------------------------------

	/**
	 * get_active_colors()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_active_colors()
	{
		$data = array();

		$this->db->select('id, name');
		$this->db->where('status','active');

		$query = $this->db->get('colors');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[$row['id']] = $row['name'];
			}
		}

		$query->free_result();  

		return $data; 
	}

	// --------------------------------------------------------------------

	/**
	 * create_color()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function create_color()
	{
		$data = array( 
			'name'		=> $this->input->post('name', TRUE),
			'status'	=> $this->input->post('status', TRUE)	
		);

		$this->db->insert('colors', $data);	 
	}

	// --------------------------------------------------------------------

	/**
	 * update_color()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function update_color()
	{
		$data = array( 
			'name'		=> $this->input->post('name', TRUE),
			'status'	=> $this->input->post('status', TRUE)
		);

		$this->db->where('id', $this->input->post('id', TRUE));
		$this->db->update('colors', $data); 
	}

	// --------------------------------------------------------------------

	/**
	 * delete_color()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete_color($id)
	{
		$data = array('status' => 'inactive');

		$this->db->where('id', $id);
		$this->db->update('colors', $data);
	}

}


// ------------------------------------------------------------------------
/* End of file colors_model.php */
/* Location: ./application/models/colors_model.php */