<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:28:23 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/models/orders_model.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Orders_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * update_cart()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	void
	 */
	public function update_cart($prod_id, $full_product)
	{
		// pull in existing cart first!
		$cart = $this->session->userdata('cart');

		$product_id = $prod_id;

		$total_price = 0;

		if (count($full_product))
		{
			if (isset($cart[$product_id]))
			{
				$prev_ct	= $cart[$product_id]['count'];
				$prev_name	= $cart[$product_id]['name'];
				$prev_price	= $cart[$product_id]['price'];

				$cart[$product_id] = array(
					'name'	=> $prev_name,
					'price'	=> $prev_price,
					'count'	=> $prev_ct + 1
				);
			}
			else
			{
				$cart[$product_id] = array(
					'name'	=> $full_product['name'],
					'price'	=> $this->format_currency($full_product['price']),
					'count'	=> 1
				);
			}

			foreach ($cart as $id => $product)
			{
				$total_price += $product['price'] * $product['count'];
			}

			$this->session->set_userdata('total_price', $this->format_currency($total_price));
			$this->session->set_userdata('cart', $cart);
			
			$this->session->set_flashdata('conf_msg', "We've added this product to your cart."); 
		}
	}

	// --------------------------------------------------------------------

	/**
	 * remove_line_item()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function remove_line_item($cart_id)
	{
		$id = $cart_id;

		$total_price = 0;

		$cart = $this->session->userdata('cart');

		if (isset($cart[$id]))
		{
			unset($cart[$id]);

			foreach ($cart as $id => $product)
			{
				$total_price += $product['price'] * $product['count'];
			}		

			$this->session->set_userdata('total_price', $this->format_currency($total_price));
			$this->session->set_userdata('cart', $cart);

			echo "Product removed.";
		}
		else
		{
			echo "Product not in cart!";
		}
	}

	// --------------------------------------------------------------------

	/**
	 * update_cart_ajax()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function update_cart_ajax($id_list)
	{
		$cart = $this->session->userdata('cart');

		// split idlist on comma first
		$records = explode(',', $id_list);

		$updated = 0;

		$total_price = $this->session->userdata('total_price');

		if (count($records))
		{
			foreach ($records as $record)
			{
				if (strlen($record))
				{
					// split each record on colon
					$fields	= explode(":", $record);
					$id		= id_clean($fields[0]);
					$ct		= $fields[1];

					if ($ct > 0 && $ct != $cart[$id]['count'])
					{
						$cart[$id]['count'] = $ct;
						$updated++;
					}
					elseif ($ct == 0)
					{
						unset($cart[$id]);
						$updated++;
					}			
				}			
			}

			if ($updated)
			{
				$total_price = 0;

				foreach ($cart as $id => $product)
				{
					$total_price += $product['price'] * $product['count'];
				}

				$this->session->set_userdata('totalprice', $this->format_currency($total_price));		
				$this->session->set_userdata('cart', $cart);			

				switch ($updated)
				{
					case 0:
						$string = "No records";
						break;

					case 1:
						$string = "$updated record";
						break;

					default:
						$string = "$updated records";
						break;
				}

				echo "$string updated";

				//$this->session->set_flashdata('update_count', $string ." updated");
			}
			else
			{
				echo "No changes detected";

				//$this->session->set_flashdata('update_count', "No changes detected");
			}
		}
		else
		{
			echo "Nothing to update";

			// $this->session->set_flashdata('update_count', "Nothing to update");
		}
	}

	// --------------------------------------------------------------------

	/**
	 * verify_cart()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function verify_cart()
	{
		$cart = $this->session->userdata('cart');

		$change = FALSE;

		if (count($cart))
		{
			foreach ($cart as $id => $details)
			{
				$id_list[] = $id;
			}

			$ids = implode(",", $id_list);

			$this->db->select('id, price');
			$this->db->where("id in ($ids)");

			$query = $this->db->get('products');

			if ($query->num_rows() > 0)
			{
				foreach ($query->result_array() as $row)
				{
					$db[$row['id']] = $row['price'];
				}
			}

			foreach ($cart as $id => $details)
			{
				if (isset($db[$id]))
				{
					if ($details['price'] != $db[$id])
					{
						$details['price'] = $this->format_currency($db[$id]);

						$change = TRUE;
					}

					$final[$id] = $details;			
				}
				else
				{
					$change = TRUE;
				}
			}

			$total_price = 0;

			foreach ($final as $id => $product)
			{
				$total_price += $product['price'] * $product['count'];
			}		

			$this->session->set_userdata('total_price', $this->format_currency($total_price));
			$this->session->set_userdata('cart', $final);

			$this->session->set_flashdata('change', $change);
		}
		else
		{
			// nothing in cart!
			$this->session->set_flashdata('error', "Nothing in cart!");
		}
	}

	// --------------------------------------------------------------------

	/**
	 * format_currency()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function format_currency($number)
	{
		return number_format($number, 2, '.', ',');
	}
 
}


// ------------------------------------------------------------------------
/* End of file orders_model.php */
/* Location: ./application/models/orders_model.php */