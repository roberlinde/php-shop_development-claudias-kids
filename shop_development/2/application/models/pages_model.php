<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:28:52 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/models/pages_model.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Pages_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * get_page()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_page($id)
	{
		$data = array();

		$this->db->where('id', $id);
		$this->db->limit(1);

		$query = $this->db->get('pages');

		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}

		$query->free_result();    

		return $data;    
	}

	// --------------------------------------------------------------------

	/**
	 * get_page_path()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_page_path($path)
	{
		$data = array();

		$this->db->where('path', $path);
		$this->db->where('status', 'active');
		$this->db->limit(1);

		$query = $this->db->get('pages');

		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}

		$query->free_result();    

		return $data;    
	}

	// --------------------------------------------------------------------

	/**
	 * get_all_pages()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_all_pages()
	{
		$data = array();

		$query = $this->db->get('pages');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$query->free_result();  

		return $data; 
	}

	// --------------------------------------------------------------------

	/**
	 * add_page()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function add_page()
	{
		$data = array( 
			'name'			=> $this->input->post('name', TRUE),
			'keywords'		=> $this->input->post('keywords', TRUE),
			'description'	=> $this->input->post('description', TRUE),
			'status'		=> $this->input->post('status', TRUE),
			'path'			=> $this->input->post('path', TRUE),
			'content'		=> $this->input->post('content', TRUE),
		);

		$this->db->insert('pages', $data);	 
	}

	// --------------------------------------------------------------------

	/**
	 * update_page()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function update_page()
	{
		$data = array( 
			'name'			=> $this->input->post('name', TRUE),
			'keywords'		=> $this->input->post('keywords', TRUE),
			'description'	=> $this->input->post('description', TRUE),
			'status'		=> $this->input->post('status', TRUE),
			'path'			=> $this->input->post('path', TRUE),
			'content'		=> $this->input->post('content', TRUE),
		);

		$this->db->where('id', $this->input->post('id', TRUE));
		$this->db->update('pages', $data); 
	}

	// --------------------------------------------------------------------

	/**
	 * delete_page()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete_page($id)
	{
		$data = array('status' => 'inactive');

		$this->db->where('id', $id);
		$this->db->update('pages', $data);	
	}

}


// ------------------------------------------------------------------------
/* End of file pages_model.php */
/* Location: ./application/models/pages_model.php */