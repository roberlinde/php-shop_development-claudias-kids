<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:29:42 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/models/products_model.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Products_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * get_product()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_product($id)
	{
		$data = array();

		$options = array('id' => $id);

		$query = $this->db->get_where('products', $options, 1);

		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}

		$query->free_result();    

		return $data;    
	}

	// --------------------------------------------------------------------

	/**
	 * get_all_products()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_all_products()
	{
		$data = array();

		$query = $this->db->get('products');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$query->free_result();    

		return $data; 
	}

	// --------------------------------------------------------------------

	/**
	 * get_products_by_category()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_products_by_category($cat_id)
	{
		$data = array();

		$this->db->select('id, name, short_desc, thumb_nail');
		$this->db->where('category_id', $cat_id);
		$this->db->where('status', 'active');
		$this->db->order_by('name', 'asc');

		$query = $this->db->get('products');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$query->free_result();    

		return $data; 
	} 

	// --------------------------------------------------------------------

	/**
	 * get_products_by_group()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @param	string
	 * @return	mixed
	 */
	public function get_products_by_group($limit, $group, $skip)
	{
		$data = array();

		if ($limit == 0)
		{
			$limit = 3;
		}

		$this->db->select('id, name, short_desc, thumb_nail');
		$this->db->where('grouping', $group);
		$this->db->where('status', 'active');
		$this->db->where('id !=', $skip);
		$this->db->order_by('name', 'asc');
		$this->db->limit($limit);

		$query = $this->db->get('products');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$query->free_result();    

		return $data; 
	} 

	// --------------------------------------------------------------------

	/**
	 * get_main_feature()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_main_feature()
	{
		$data = array();

		$this->db->select("id, name, short_desc, image");
		$this->db->where('featured', 'true');
		$this->db->where('status', 'active');
		$this->db->order_by("rand()"); 
		$this->db->limit(1);

		$query = $this->db->get('products');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data = array(
					'id'			=> $row['id'],
					'name'			=> $row['name'],
					'short_desc'	=> $row['short_desc'],
					'image'			=> $row['image']
				);
			}
		}

		$query->free_result();    

		return $data; 
	}

	// --------------------------------------------------------------------

	/**
	 * get_random_products()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @return	mixed
	 */
	public function get_random_products($limit, $skip)
	{
		$data = array();
		$temp = array();

		if ($limit == 0)
		{
			$limit = 3;
		}

		$this->db->select('id, name, thumb_nail, category_id');
		$this->db->where('id !=', $skip);
		$this->db->order_by('category_id', 'asc'); 
		$this->db->limit(100);

		$query = $this->db->get('products');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$temp[$row['category_id']] = array(
					'id'			=> $row['id'],
					'name'			=> $row['name'],
					'thumb_nail'	=> $row['thumb_nail']
				);
			}
		}

		shuffle($temp);

		if (count($temp))
		{
			for ($i = 1; $i <= $limit; $i++)
			{
				$data[] = array_shift($temp);
			} 
		}

		$query->free_result();

		return $data;  
	}

	// --------------------------------------------------------------------

	/**
	 * search()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function search($term)
	{
		$data = array();

		$this->db->select('id, name, short_desc, thumb_nail');
		$this->db->like('name', $term);
		$this->db->or_like('short_desc', $term);
		$this->db->or_like('long_desc', $term);
		$this->db->order_by('name', 'asc');
		$this->db->where('status', 'active');
		$this->db->limit(50);

		$query = $this->db->get('products');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$query->free_result();

		return $data;
	}

	// --------------------------------------------------------------------

	/**
	 * add_product()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function add_product()
	{
		$data = array( 
			'name'			=> $this->input->post('name', TRUE),
			'short_desc'	=> $this->input->post('short_desc', TRUE),
			'long_desc'		=> $this->input->post('long_desc', TRUE),
			'status'		=> $this->input->post('status', TRUE),
			'grouping'		=> $this->input->post('grouping', TRUE),
			'category_id'	=> $this->input->post('category_id', TRUE),
			'featured'		=> $this->input->post('featured', TRUE),
			'price'			=> $this->input->post('price', TRUE),
		);

		if ($_FILES)
		{
			$config['upload_path']		= './assets/images/';
			$config['allowed_types']	= 'gif|jpg|png';
			$config['max_size']			= '200';
			$config['remove_spaces']	= TRUE;
			$config['overwrite']		= FALSE;
			$config['max_width']		= '0';
			$config['max_height']		= '0';

			$this->load->library('upload', $config);	
	
			if (strlen($_FILES['image']['name']))
			{
				if ( ! $this->upload->do_upload('image'))
				{
					$this->upload->display_errors();
					exit();
				}

				$image = $this->upload->data();

				if ($image['file_name'])
				{
					$data['image'] = "/assets/images/" . $image['file_name'];		
				}
			}		

			if (strlen($_FILES['thumb_nail']['name']))
			{
				if ( ! $this->upload->do_upload('thumb_nail'))
				{
					$this->upload->display_errors();
					exit();
				}

				$thumb = $this->upload->data();

				if ($thumb['file_name'])
				{
					$data['thumb_nail'] = "/assets/images/" . $thumb['file_name'];		
				}
			}
		}

		$this->db->insert('products', $data);	
	
		$new_product_id = $this->db->insert_id();
	
		if (count($_POST['colors']))
		{
			foreach ($_POST['colors'] as $value)
			{
				$data = array(
					'product_id'	=> $new_product_id,
					'color_id'		=> $value,
				);

				$this->db->insert('products_colors', $data);
			}
		}

		if (count($_POST['sizes']))
		{
			foreach ($_POST['sizes'] as $value)
			{
				$data = array(
					'product_id'	=> $new_product_id, 
					'size_id'		=> $value
				);

				$this->db->insert('products_sizes', $data);
			}
		}	
	}

	// --------------------------------------------------------------------

	/**
	 * update_product()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function update_product()
	{
		$data = array( 
			'name'			=> $this->input->post('name', TRUE),
			'short_desc'	=> $this->input->post('short_desc', TRUE),
			'long_desc'		=> $this->input->post('long_desc', TRUE),
			'status'		=> $this->input->post('status', TRUE),
			'grouping'		=> $this->input->post('grouping', TRUE),
			'category_id'	=> $this->input->post('category_id', TRUE),
			'featured'		=> $this->input->post('featured', TRUE),
			'price'			=> $this->input->post('price', TRUE),
		);

		if ($_FILES)
		{
			$config['upload_path']		= './assets/images/';
			$config['allowed_types']	= 'gif|jpg|png';
			$config['max_size']			= '200';
			$config['remove_spaces']	= TRUE;
			$config['overwrite']		= FALSE;
			$config['max_width']		= '0';
			$config['max_height']		= '0';

			$this->load->library('upload', $config);	
	
			if (strlen($_FILES['image']['name']))
			{
				if ( ! $this->upload->do_upload('image'))
				{
					$this->upload->display_errors();
					exit();
				}

				$image = $this->upload->data();

				if ($image['file_name'])
				{
					$data['image'] = "/assets/images/" . $image['file_name'];		
				}
			}

			if (strlen($_FILES['thumbnail']['name']))
			{
				if ( ! $this->upload->do_upload('thumbnail'))
				{
					$this->upload->display_errors();
					exit();
				}

				$thumb = $this->upload->data();

				if ($thumb['file_name'])
				{
					$data['thumbnail'] = "/assets/images/" . $thumb['file_name'];		
				}
			}
		}

		$this->db->where('id', $this->input->post('id', TRUE));
		$this->db->update('products', $data);	
 
		$this->db->where('product_id', $this->input->post('id', TRUE));
		$this->db->delete('products_colors');

		$this->db->where('product_id', $this->input->post('id', TRUE));
		$this->db->delete('products_sizes'); 

		if (count($_POST['colors']))
		{
			foreach ($_POST['colors'] as $value)
			{
				$data = array(
					'product_id'	=> $this->input->post('id', TRUE),
					'color_id'		=> $value,
				);

				$this->db->insert('products_colors', $data);
			}
		}

		if (count($_POST['sizes']))
		{
			foreach ($_POST['sizes'] as $value)
			{
				$data = array(
					'product_id'	=> $this->input->post('id', TRUE),
					'size_id'		=> $value,
				);

				$this->db->insert('products_sizes', $data);
			}
		}		
	}

	// --------------------------------------------------------------------

	/**
	 * delete_product()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete_product($id)
	{
		$data = array('status' => 'inactive');

		$this->db->where('id', $id);
		$this->db->update('products', $data);	
	}
	
	// --------------------------------------------------------------------

	/**
	 * batch_update()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function batch_update()
	{
		if (count($this->input->post('p_id')))
		{
			$data = array(
				'category_id'	=> $this->input->post('category_id'),
				'grouping'		=> $this->input->post('grouping'),
			);

			$idlist = implode(",", array_values($this->input->post('p_id', TRUE)));

			$where = "id in ($id_list)";

			$this->db->where($where);
			$this->db->update('products', $data);

			$this->session->set_flashdata('message', 'Products updated');
		}
		else
		{
			$this->session->set_flashdata('message', 'Nothing to update!');
		}   
	}

	// --------------------------------------------------------------------

	/**
	 * export_csv()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function export_csv()
	{
		$this->load->dbutil();

		$query = $this->db->query("select * from products");

		return $this->dbutil->csv_from_result($query, ",", "\n");
	}

	// --------------------------------------------------------------------

	/**
	 * import_csv()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function import_csv()
	{
		$config['upload_path']		= './assets/csv/';
		$config['allowed_types']	= 'csv';
		$config['max_size']			= '2000';
		$config['remove_spaces']	= TRUE;
		$config['overwrite']		= TRUE;

		$this->load->library('upload', $config);
		$this->load->library('CSV_Reader'); 

		if ( ! $this->upload->do_upload('csv_file'))
		{
			$this->upload->display_errors();
			exit();
		}

		$csv  = $this->upload->data();
		$path = $csv['full_path'];

		return $this->csv_reader->parse_file($path);
	}

	// --------------------------------------------------------------------

	/**
	 * csv_to_db()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function csv_to_db()
	{
		unset($_POST['submit']);
		unset($_POST['csvgo']);

		foreach ($_POST as $line => $data)
		{
			if (isset($data['id']))
			{
				$this->db->where('id', $data['id']);

				unset($data['id']);

				$this->db->update('products', $data);
			}
			else
			{
				$this->db->insert('products', $data);
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * reassign_products()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function reassign_products()
	{
		$data = array('category_id' => $this->input->post('categories'));

		$idlist = implode(",", array_keys($this->session->userdata('orphans')));

		$where = "id in ($id_list)";

		$this->db->where($where);
		$this->db->update('products', $data);
	} 

	// --------------------------------------------------------------------

	/**
	 * get_assigned_colors()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_assigned_colors($id)
	{
		$data = array();

		$this->db->select('color_id');
		$this->db->where('product_id', $id);

		$query = $this->db->get('products_colors');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row['color_id'];
			}
		}

		$query->free_result();    

		return $data; 	
	}

	// --------------------------------------------------------------------

	/**
	 * get_assigned_sizes()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_assigned_sizes($id)
	{
		$data = array();

		$this->db->select('size_id');
		$this->db->where('product_id', $id);

		$query = $this->db->get('products_sizes');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row['size_id'];
			}
		}

		$query->free_result();    

		return $data; 	
	} 

}


// ------------------------------------------------------------------------
/* End of file products_model.php */
/* Location: ./application/models/products_model.php */