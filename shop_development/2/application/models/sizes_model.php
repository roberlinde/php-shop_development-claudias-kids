<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:30:11 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/models/sizes_model.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Sizes_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * get_size()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_size($id)
	{
		$data = array();

		$options = array('id' => $id);

		$query = $this->db->get_where('sizes', $options, 1);

		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}

		$query->free_result();    

		return $data;    
	}

	// --------------------------------------------------------------------

	/**
	 * get_all_sizes()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_all_sizes()
	{
		$data = array();

		$query = $this->db->get('sizes');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$query->free_result();  

		return $data; 
	}

	// --------------------------------------------------------------------

	/**
	 * get_active_sizes()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_active_sizes()
	{
		$data = array();

		$this->db->select('id, name');
		$this->db->where('status','active');

		$query = $this->db->get('sizes');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[$row['id']] = $row['name'];
			}
		}

		$query->free_result();

		return $data; 
	}  

	// --------------------------------------------------------------------

	/**
	 * create_size()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function create_size()
	{
		$data = array( 
			'name'		=> $this->input->post('name', TRUE),
			'status'	=> $this->input->post('status', TRUE)	
		);

		$this->db->insert('sizes', $data);	 
	}

	// --------------------------------------------------------------------

	/**
	 * update_size()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function update_size()
	{
		$data = array( 
			'name'		=> $this->input->post('name', TRUE),
			'status'	=> $this->input->post('status', TRUE)
		);

		$this->db->where('id', $this->input->post('id', TRUE));
		$this->db->update('sizes', $data); 
	}

	// --------------------------------------------------------------------

	/**
	 * delete_size()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete_size($id)
	{
		$data = array('status' => 'inactive');

		$this->db->where('id', $id);
		$this->db->update('sizes', $data);	
	}

}


// ------------------------------------------------------------------------
/* End of file sizes_model.php */
/* Location: ./application/models/sizes_model.php */