<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created with PhpDesigner7.
 * Created by: The Development Team.
 * User: Raymond King aka: (InsiteFX)
 * Date: 1/15/2012
 * Time: 10:30:42 AM
 * @copyright 1/15/2012 by Raymond L King.
 *
 * Class name: ./application/models/subscribers_model.php
 *
 * To change this template use File | Settings | File Templates.
 */

class Subscribers_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * __construct()
	 *
	 * Constructor	PHP 5+	NOTE: Not needed if not setting values!
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * get_subscriber()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	mixed
	 */
	public function get_subscriber($id)
	{
		$this->db->where('id', $id);
		$this->db->limit(1);

		$query = $this->db->get_where('subscribers');

		if ($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}

		$query->free_result();    

		return $data;    
	}

	// --------------------------------------------------------------------

	/**
	 * get_all_subscribers()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	mixed
	 */
	public function get_all_subscribers()
	{
		$data = array();

		$query = $this->db->get('subscribers');

		if ($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
			{
				$data[] = $row;
			}
		}

		$query->free_result();  

		return $data; 
	}

	// --------------------------------------------------------------------

	/**
	 * create_subscriber()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function create_subscriber()
	{
		$this->db->where('email', $this->input->post('email', TRUE));
		$this->db->from('subscribers');

		$ct = $this->db->count_all_results();

		if ($ct == 0)
		{
			$data = array( 
				'name'	=> $this->input->post('name', TRUE),
				'email'	=> $this->input->post('email', TRUE)	
			);

			$this->db->insert('subscribers', $data);	 
		}
	}

	// --------------------------------------------------------------------

	/**
	 * update_subscriber()
	 *
	 * Description:
	 *
	 * @access	public
	 * @return	void
	 */
	public function update_subscriber()
	{
		$data = array(
			'name'	=> $this->input->post('name', TRUE),
			'email'	=> $this->input->post('email', TRUE)
		);

		$this->db->where('id', $this->input->post('id', TRUE));
		$this->db->update('subscribers', $data); 
	}

	// --------------------------------------------------------------------

	/**
	 * delete_subscriber()
	 *
	 * Description:
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function delete_subcriber($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('subscribers');	
	}

}


// ------------------------------------------------------------------------
/* End of file subscribers_model.php */
/* Location: ./application/models/subscribers_model.php */