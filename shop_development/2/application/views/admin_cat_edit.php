<h1><?php echo $title;?></h1>

<?php
	echo form_open('admin/categories/edit');
		echo "<p><label for='catname'>Name</label><br/>";
		$data = array('name' => 'name', 'id' => 'catname', 'size' => 25, 'value' => $category['name']);
		echo form_input($data) . "</p>";

		echo "<p><label for='short'>Short Description</label><br/>";
		$data = array('name' => 'short_desc', 'id' => 'short', 'size' => 40, 'value' => $category['short_desc']);
		echo form_input($data) . "</p>";

		echo "<p><label for='long'>Long Description</label><br/>";
		$data = array('name' => 'long_desc', 'id' => 'long', 'rows' => 5, 'cols' => '40', 'value' => $category['long_desc']);
		echo form_textarea($data) . "</p>";

		echo "<p><label for='status'>Status</label><br/>";
		$options = array('active' => 'active', 'inactive' => 'inactive');
		echo form_dropdown('status', $options, $category['status']) . "</p>";

		echo "<p><label for='parent'>Category Parent</label><br/>";
		echo form_dropdown('parent_id', $categories, $category['parent_id']) . "</p>";

		echo form_hidden('id', $category['id']);
		echo form_submit('submit', 'update category');
	echo form_close();
?>