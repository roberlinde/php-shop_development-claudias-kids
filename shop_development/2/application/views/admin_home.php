<br />
<ul>
	<li><b><?php echo anchor("admin/categories/","Manage Categories");?>.</b><br />
		<p>Create, edit, delete and manage product categories on your online store.</p>
	</li><br />
	<li><b><?php echo anchor("admin/products/","Manage Products");?>.</b><br />
		<p>Create, edit, delete and manage products on your online store.</p>
	</li><br />
	<li><b><?php echo anchor("admin/pages/","Manage Pages");?>.</b><br />
		<p>Create, edit, delete and manage pages on your online store.</p>
	</li><br />
	<li><b><?php echo anchor("admin/colors/","Manage Colors");?>.</b><br />
		<p>Create, edit, delete and manage colors available to products.</p>
	</li><br />
	<li><b><?php echo anchor("admin/sizes/","Manage Sizes");?>.</b><br />
		<p>Create, edit, delete and manage sizes available to products.</p>
	</li><br />
	<li><b><?php echo anchor("admin/subscribers/","Manage Subscribers");?>.</b><br />
		<p>Manage subscribers and send out emails.</p>
	</li><br />
	<li><b><?php echo anchor("admin/admins/","Manage Users");?>.</b><br />
		<p>Create, edit, delete and manage users who can access this dashboard.</p>
	</li><br />
	<li><b><?php echo anchor("admin/dashboard/logout/","Logout");?>.</b><br />
		<p>Exit this dashboard when you're done.</p>
	</li><br />
</ul>
