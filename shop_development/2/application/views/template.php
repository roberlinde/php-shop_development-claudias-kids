<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<title><?php echo $title; ?></title>

	<base href="<?php echo base_url();?>" />

	<link href="<?php echo base_url();?>assets/css/default.css" rel="stylesheet" type="text/css" />

	<noscript>
		Javascript is not enabled! Please turn on Javascript to use this site.
	</noscript>

	<script type="text/javascript">
		//<![CDATA[
			base_url = '<?php echo base_url();?>';
		//]]>
	</script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/prototype.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/scriptaculous.js" ></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/customtools.js" ></script>

	<!--[if IE]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
</head>

<body>

	<div id="wrapper">

		<div id="header">
			<?php $this->load->view('header');?>
		</div>
  
		<div id="nav">
			<?php $this->load->view('navigation');?>
		</div>

		<div id="main">
			<?php $this->load->view($main);?>
		</div>
  
		<div id="footer"> 
			<?php $this->load->view('footer');?>
		</div>

	</div>

</body>

</html>