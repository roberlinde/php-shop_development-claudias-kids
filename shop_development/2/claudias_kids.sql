-- phpMyAdmin SQL Dump
-- version 3.4.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 15, 2012 at 03:52 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `claudiakids`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','inactive','banned','deleted') COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`user_name`,`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `user_name`, `email`, `status`, `password`) VALUES
(1, 'admin', 'myerman@gmail.com', 'active', '6306b813fbb6353d0eafff0dee1939eb25888dd26e515bece15da3989bac943f479d59000c4987726709f68d0df56f1a36b57085f4ed0f64f3b8635ba2938fad');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `long_desc` text COLLATE utf8_unicode_ci,
  `status` enum('active','inactive','deleted') COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `short_desc`, `long_desc`, `status`, `parent_id`) VALUES
(1, 'shoes', 'Shoes for boys and girls.', '', 'active', 7),
(2, 'shirts', 'Shirts and blouses!', '', 'active', 7),
(3, 'pants', 'Stylish, durable pants for play or school.', '', 'active', 7),
(4, 'dresses', 'Pretty dresses for the apple of your eye.', '', 'inactive', 7),
(5, 'toys', 'Toys that are fun and mentally stimulating at the same time.', '', 'active', 8),
(6, 'games', 'Fun for the whole family.', '', 'active', 8),
(7, 'clothes', 'Clothes for school and play.', '', 'active', 0),
(8, 'fun', 'It''s time to unwind!', '', 'active', 0),
(9, 'test', 'testing', 'Testing!!!!', 'inactive', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('936219bcbf25b45e9cd669a2c15a9260', '127.0.0.1', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', 1326568103, 'a:3:{s:9:"user_data";s:0:"";s:11:"total_price";s:6:"139.80";s:4:"cart";a:2:{i:14;a:3:{s:4:"name";s:20:"Long-sleeved t-shirt";s:5:"price";s:5:"29.95";s:5:"count";i:2;}i:3;a:3:{s:4:"name";s:6:"Game 3";s:5:"price";s:5:"39.95";s:5:"count";i:2;}}}'),
('4fafcbe6b8ef915462129931be7d0a0b', '127.0.0.1', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', 1326639783, '');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','inactive','deleted') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `status`) VALUES
(1, 'red', 'active'),
(2, 'green', 'active'),
(3, 'blue', 'active'),
(4, 'yellow', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci,
  `status` enum('active','inactive','deleted') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `keywords`, `description`, `path`, `content`, `status`) VALUES
(1, 'About Us', 'about us', 'about us page', 'about_us', '<p>Sample About Us information.\r\nThis is a test for the About Us.\r\n</p>', 'active'),
(2, 'Privacy', 'privacy', 'privacy', 'privacy', 'Privacy information', 'active'),
(3, 'Contact', 'contact', 'contact', 'contact', 'Contact Information goes here', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `long_desc` text COLLATE utf8_unicode_ci,
  `thumb_nail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grouping` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive','deleted') COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `featured` enum('true','false') COLLATE utf8_unicode_ci NOT NULL,
  `price` float(4,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `short_desc`, `long_desc`, `thumb_nail`, `image`, `grouping`, `status`, `category_id`, `featured`, `price`) VALUES
(1, 'Game 1', 'This is a very good game.', 'What a product! You''ll love the way your kids will play with this game all day long. It''s terrific!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 3, 'true', 19.95),
(2, 'Game 2', 'This is a very good game.', 'What a product! You''ll love the way your kids will play with this game all day long. It''s terrific!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 3, 'true', 19.95),
(3, 'Game 3', 'This is a very good game.', 'What a product! You''ll love the way your kids will play with this game all day long. It''s terrific!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 1, 'true', 39.95),
(4, 'Toy 1', 'This is a very good toy.', 'What a product! You''ll love the way your kids will play with this game all day long. It''s terrific!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 1, 'true', 9.95),
(5, 'Toy 2', 'This is a very good toy.', 'What a product! You''ll love the way your kids will play with this game all day long. It''s terrific!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 6, 'false', 23.95),
(6, 'Shoes 1', 'This is a very good pair of shoes.', 'These shoes will last forever!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 6, 'true', 23.95),
(7, 'Shoes 2', 'This is a very good pair of shoes.', 'These shoes will last forever!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 1, 'false', 23.95),
(8, 'Shirt 1', 'Nice shirt!', 'A stylish shirt for school!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 2, 'true', 23.95),
(9, 'Shirt 2', 'Nice shirt!', 'A stylish shirt for school!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 2, 'false', 23.95),
(10, 'Dress 1', 'Nice dress!', 'A stylish dress just in time for school!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 2, 'true', 33.95),
(11, 'Dress 2', 'Nice dress!', 'A stylish dress just in time for school!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 2, 'true', 43.95),
(12, 'Pants 1', 'Nice pair of pants!', 'A stylish pair of pants just in time for school!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blob', 'active', 3, 'true', 33.95),
(13, 'test123', 'test!!', 'test!!!!', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'xyz', 'active', 1, 'false', 10.95),
(14, 'Long-sleeved t-shirt', 'Very comfy!', 'A great t-shirt for cold autumn days.', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'blah', 'active', 2, 'true', 29.95),
(15, 'Shoes Testing', 'test', 'test', 'assets/images/dummy-thumb.jpg', 'assets/images/dummy-main.jpg', 'adfasdf', 'active', 1, 'true', 1.29);

-- --------------------------------------------------------

--
-- Table structure for table `products_colors`
--

DROP TABLE IF EXISTS `products_colors`;
CREATE TABLE IF NOT EXISTS `products_colors` (
  `product_id` int(11) unsigned NOT NULL,
  `color_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`color_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_colors`
--

INSERT INTO `products_colors` (`product_id`, `color_id`) VALUES
(15, 2),
(15, 3),
(16, 3);

-- --------------------------------------------------------

--
-- Table structure for table `products_sizes`
--

DROP TABLE IF EXISTS `products_sizes`;
CREATE TABLE IF NOT EXISTS `products_sizes` (
  `product_id` int(11) unsigned NOT NULL,
  `size_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`size_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_sizes`
--

INSERT INTO `products_sizes` (`product_id`, `size_id`) VALUES
(15, 1),
(15, 2),
(15, 3),
(15, 4),
(16, 4);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

DROP TABLE IF EXISTS `sizes`;
CREATE TABLE IF NOT EXISTS `sizes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` enum('active','inactive','deleted') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `status`) VALUES
(1, 'S', 'active'),
(2, 'M', 'active'),
(3, 'L', 'active'),
(4, 'XL', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `name`, `email`) VALUES
(1, 'Tom Myer', 'myerman@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
